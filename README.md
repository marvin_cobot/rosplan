# ROSPlan

ROSPlan is a ROS module that provides a service for solving planning problems. It generates action plans for autonomous systems based on the task planning system.

For more information on automated planning, please refer to the [automated planning wiki](https://planning.wiki/).

## Installation

### Dependencies

This module depends on the following packages:

- [Ubuntu 20.04](https://releases.ubuntu.com/20.04/)
- [ROS noetic](http://wiki.ros.org/noetic)
- [rospy](https://wiki.ros.org/rospy)

Make sure you have installed these dependencies before proceeding.

### Installation from Source

1. Clone this repository into your ROS workspace:

   ```bash
   cd path/to/your/ros/workspace/src
   git clone https://gitlab.com/marvin_cobot/rosplan.git```

2. Build the package using catkin:

    ```bash
    cd path/to/your/ros/workspace
    catkin build

3. Build planner

  ```bash
  cd path/to/your/ros/workspace/rosplan
  cd downward
  ./build release

### Usage

To use ROSPlan, follow these steps:

1. Ensure that the ROS master (roscore) is running:

    ```roscore```

2. Launch the planner node:

    ```rosrun rosplan planner.py```

Make sure the planner node is running before calling the planning problem resolution service.

3. Call the planning problem resolution service:

    ```rosservice call /rosplan/solve_pddl "problem: { domain: 'your_domain.pddl', instance: 'your_instance.pddl' }"```

The **/rosplan/solve_pddl** service uses the **SolvePddlCmd.srv** service type, which contains the following fields:

* **rosplan/PddlProblem problem**: This is the PDDL problem to solve. The PddlProblem message contains the following fields:
    * **string domain**: The path to the PDDL domain file.
    * **string instance**: The path to the PDDL instance file.
* **rosplan/PddlPlan plan**: This is the generated action plan if found. The PddlPlan message contains an array of actions (rosplan/PddlAction[] actions).
* **bool success**: Indicates whether the problem resolution was successful.

Make sure to fill in the domain and instance fields with the appropriate paths to the domain and instance PDDL files.
