#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

"""
Node: rosplan
======================

Description:
------------
This node implements a PDDL planner to solve planning problems. It uses a service to receive planning requests
and executes an external planner (Fast Downward) to generate a solution. The result of the planning is a sequence
of PDDL actions forming the plan.

Publications:
-------------
None.

Subscriptions:
--------------
None.

Services:
---------
- `/rosplan/solve_pddl`: [SolvePddlCmd] - A service that solves a given PDDL problem by specifying a domain and
  an instance. This service returns a plan containing the actions to execute and a success flag.
Usage:
------
This node is designed to work with the `/rosplan/solve_pddl` service. When a client calls this service,
it provides a domain and instance PDDL file to solve a planning problem. The node then executes the planner
and returns a sequence of actions forming the plan, or indicates failure if planning was unsuccessful.
"""

__author__ = "Maxence Grand"
__credits__ = ["Maxence Grand, Belal Hmedan"]
__license__ = "LGPLv2.1"
__version__ = "1.0.0"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"

import rospy
import json
import rospkg
import subprocess
import time
import os

from rosplan.msg import PddlAction, PddlPlan, PddlProblem
from rosplan.srv import SolvePddlCmd, CheckPddlCmd

class PddlPlanner:
    """The Pddl Planner"""

    def __init__(self):
        """Constructs Pdll Planner"""
        rospack = rospkg.RosPack()
        self.path = rospack.get_path("rosplan")
        self.planner_path = self.path + "/downward/fast-downward.py"
        self.plan_path    = self.path + "/log/plan.txt"
        self.out_path    = self.path + "/log/log_out.txt"
        self.err_path    = self.path + "/log/log_err.txt"


    def solve(self, domain, instance):
        """Solves the given planning problem.

        This method runs the PDDL planner on the specified domain and instance,
        generates a plan, and measures the planning time.

        @param domain: The PDDL domain file to be used.
        @type domain: str
        @param instance: The PDDL problem instance file to be solved.
        @type instance: str
        @return: A tuple containing the generated PDDL plan and a success flag.
        @rtype: tuple(PddlPlan, bool)
        """
        domain = domain
        problem = instance

        self.tic = time.time()
        actions = []

        """
        https://stackoverflow.com/a/75515712
        """
        search_options = "seq-opt-lmcut"
        cmd = [
            "python3",
            self.planner_path,
            "--alias",
            search_options,
            "--plan-file",
            self.plan_path,
            domain,
            problem,
        ]

        try:
            with open(self.out_path, "w") as out:
                with open(self.err_path, "w") as err:
                    subprocess.run(cmd, stdout=out, stderr=err).check_returncode()
            if os.path.isfile(self.plan_path):
                with open(self.plan_path, encoding="utf-8") as f:
                    plan_msg = f.read()
                    for action in plan_msg.splitlines()[:-1]:
                        pddl_act = PddlAction()
                        pddl_act.pddl_action = action[1:-1]
                        actions.append(pddl_act)
            self.toc = time.time()
            rospy.loginfo("Planning_time is: {} seconds".format(round(self.toc - self.tic, 3)))
        except Exception:
            self.toc = time.time()
            rospy.loginfo("Abort")
            rospy.loginfo("Planning_time is: {} seconds".format(round(self.toc - self.tic, 3)))
            return PddlPlan(), False
        self.tic = self.toc
        plan = PddlPlan()
        plan.actions = actions
        return plan, True

    def service_solve(self, req):
        """Service handler for solving a planning problem.

        This method is called when a new planning problem is received via a service
        request. It processes the request and returns the generated plan along with
        a success flag.

        @param req: The service request containing the planning problem.
        @type req: CheckPlanCmd
        @return: A tuple containing the generated PDDL plan and a success flag.
        @rtype: tuple(PddlPlan, bool)
        """
        rospy.loginfo("New probem received")
        plan, success = self.solve(req.problem.domain, req.problem.instance)
        return plan, success

def main():
    planner = PddlPlanner()
    rospy.init_node("rosplan", anonymous=True)
    rospy.loginfo("============ Node rosplan Initialized")
    serv_pddl_solve = rospy.Service('/rosplan/solve_pddl', SolvePddlCmd, planner.service_solve)
    rospy.loginfo("Service /rosplan/solve_pddl ready")

    rospy.spin()

if __name__ == "__main__":
    main()
